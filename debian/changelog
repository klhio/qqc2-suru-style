qqc2-suru-style (0.20230206) unstable; urgency=medium

  * Upstream-provided Debian package for qqc2-suru-style. See upstream
    ChangeLog for recent changes.

 -- UBports developers <developers@ubports.com>  Mon, 06 Feb 2023 15:38:45 +0100

qqc2-suru-style (0.20220527) xenial; urgency=medium

  [ Daniel Kutka ]
  * Add support for icons and 5.12 fixes (#56)
  * Fix Cannot override FINAL property error for sliders
  * Add support for icons for Buttons and Delegates
  * Fix icon colors
  [ Rodney Dawes ]
  * Move the internal implementation items to impl subdir
    Fixes #53
  * Add missing QtQuick.Controls 2 import
  [ Brian Douglass ]
  * Fixes for Qt 5.12
  * Fixed the combo box's editable state
  * Added nasty hack to allow clickable to work again
  * Follow upstream's default for word wrap
  * Set the default dial background width to match the material design size
  * Removed problematic width setting in the Label element
  * Fixed dp unit when HighDpiScaling is active
  * Fixed the size and color of the svg spinner
  * Respect the GRID_UNIT_PX env var
  * Added clickable setup for easier testing on device
  * Added detection for the Ubuntu Touch dark theme
  [ dano6 ]
  * Fix ComboBox for 5.12
  * Port CheckIndicator and SpinBox to QT 5.12
  [ Marius Gripsgard ]
  * debian/rules: allow building with crossbuilder
  [ Alberto Mardegan ]
  * debian/rules: do not override existing QT_SELECT value
    This allows the project to be build in crossbuilder.
  [ Johannes Renkl ]
  * Removed type registration of Label (#41)
  * Added tertiaryForegroundColor and defined the foreground colors once
  [ Luca Weiss ]
  * Fix missing .qml files in Qt 5.9 (#40)
  * Port C++ parts to Qt 5.12 (#38)
  * Add some QML type registrations
  [ Joan CiberSheep ]
  * Set Elevation shadow to grey
  * Fix to Sliders handler light and dark mode
  * Restore Page background
  * Add Scalable Spinner Icon
  * Simpler Transitions for StackView
  * Remove Page background
  [ Chris Clime ]
  * add foregroundColor to SpinBox
  [ Michele ]
  * added secondaryForegroundColor
  [ JBB ]
  * Update control
  * debian: Add qtdeclarative5-private-dev and qtdeclarative5-dev-tools deps
  * debian: Add missing pkg-kde-tools dependency

 -- Guido Berhoerster <guido+gitlab.com@berhoerster.name>  Fri, 27 May 2022 10:23:54 +0200

qqc2-suru-style (0.20180223ubuntu1) xenial; urgency=medium

  [ JBBgameich ]
  * Initial release

  [ Dan Chapman ]
  * Add jenkinsfile and import to ubports repo

 -- Dan Chapman <dan@ubports.com>  Fri, 04 May 2018 16:03:49 +0000
